<?php

namespace app\models;

// Classe responsável por
class Load
{
  public static function file($file)
  {
    // concatena o caminho de diretorios até o projeto com o caminho + arquivo enviado por parametro
    // exemplo:  "home/usuario/projeto-slim-twig" + "/app/functions/twig.php"
    $file = path().$file;

    if(!file_exists($file))
    {
      throw new \Exception("Esse arquivo não existe: {$file}");
    }

    return require $file;
  }
}
