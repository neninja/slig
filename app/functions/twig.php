<?php

/*
* declaração das funções existentes
*/

// $teste = new \Twig_SimpleFunction('teste', function(){
//     echo 'função twig';
// });
// no template: {{ teste() }}


// $file_exists = new \Twig_SimpleFunction('file_exists', function($file){
//     return file_exists($file);
// });
// no template: {{ file_exists(teste.php) }}

// lista de funções que o template do twig pode utilizar
return [
    // $file_exists,
    // $teste
];
